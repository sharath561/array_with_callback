const items = require("../items");
const map = require("../map");

function callBack(element) {
  return element;
}

module.exports = callBack;

console.log(map(items, callBack));
