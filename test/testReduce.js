const items = require("../items");
const reduce = require("../reduce");

function callBack(acc, curr) {
  return acc + curr;
}

console.log(reduce(items, callBack));
