function find(data, cb) {
  if (Array.isArray(data) && cb != undefined) {
    for (let item = 0; item < data.length; item++) {
      if (cb(data[item])) {
        return data[item];
      }
    }
    return undefined;
  }
  else{
    return undefined;
  }
}
module.exports = find;
