function reduce(data, cb, startingValue) {
  if (Array.isArray(data) && cb != undefined) {
    let acc = 0;
    let curr = 0;

    if (startingValue == undefined) {
      acc = data[0];
      curr = 1;
    } else {
      acc = startingValue;
      curr = 0;
    }

    for (let item = curr; item < data.length; item++) {
      acc = cb(acc, data[item]);
    }

    return acc;
  }
  else{
    return undefined
  }
}

module.exports = reduce;
