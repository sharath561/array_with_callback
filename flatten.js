const nestedArray = [1, [2], [[3]], [[[4]]]];
const result = [];
function flatten(data) {
  if (Array.isArray(data)) {
    for (let item = 0; item < data.length; item++) {
      if (Array.isArray(data[item])) {
        flatten(data[item]);
      } else {
        result.push(data[item]);
      }
    }
    return;
  } else {
    return [];
  }
}
flatten(nestedArray);
console.log(result);
