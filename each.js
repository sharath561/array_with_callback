function each(data, cb) {
  if (Array.isArray(data) && cb != undefined) {
    const result = [];

    for (let item = 0; item < data.length; item++) {
      result.push(cb(data, item));
    }

    return result;
  }
  else{

    return result
  }
}
module.exports = each;
