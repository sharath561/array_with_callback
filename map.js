function map(data, cb) {
  if (Array.isArray(data) && cb != undefined) {
    const result = [];

    for (let element = 0; element < data.length; element++) {
      result.push(cb(data[element]));
    }
    return result;
  }
  else{
    return []
  }
}

module.exports = map;
