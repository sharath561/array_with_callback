function filter(data, cb) {
  if (Array.isArray(data) && cb != undefined) {
    const result = [];

    for (let item = 0; item < data.length; item++) {
      if (cb(data[item])) {
        result.push(data[item]);
      }
    }

    return result;
  } else {
    return [];
  }
}

module.exports = filter;
